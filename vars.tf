variable "organization_id" {
  type = string
}

variable "cloud_name" {
  type = string
}

variable "billing_account_id" {
  type = string
}

variable "mgmt_folder_name" {
  type = string
}

variable "dev_folder_name" {
  type = string
}

variable "prod_folder_name" {
  type = string
}

variable "tfstatesa_name" {
  type = string
}

variable "tfstatebucket_name" {
  type = string
}

variable "tfstatebucket_maxsize" {
  type = number
}

variable "tfmgmtsa_name" {
  type = string
}

variable "write_keys_to_file" {
  type    = bool
  default = false
}

terraform {
  backend "s3" {
    key                         = "global/cloud/terraform.tfstate"
    bucket                      = "do230819-tfstate-bucket"
    endpoint                    = "storage.yandexcloud.net"
    region                      = "ru-central1"
    skip_region_validation      = true
    skip_credentials_validation = true

    # AWS_ACCESS_KEY_ID
    # access_key = ""
    # AWS_SECRET_ACCESS_KEY
    # secret_key = ""
  }

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.82.0"
    }
  }
}

provider "yandex" {
  organization_id = var.organization_id
}

resource "yandex_resourcemanager_cloud" "cloud" {
  name            = var.cloud_name
  organization_id = var.organization_id
}

resource "yandex_billing_cloud_binding" "cloudbilling" {
  cloud_id           = yandex_resourcemanager_cloud.cloud.id
  billing_account_id = var.billing_account_id
}

resource "time_sleep" "wait_10_seconds" {
  depends_on      = [yandex_billing_cloud_binding.cloudbilling]
  create_duration = "10s"
}

# Folders

resource "yandex_resourcemanager_folder" "mgmt" {
  cloud_id   = yandex_resourcemanager_cloud.cloud.id
  name       = var.mgmt_folder_name
  depends_on = [time_sleep.wait_10_seconds]
}

resource "yandex_resourcemanager_folder" "dev" {
  cloud_id   = yandex_resourcemanager_cloud.cloud.id
  name       = var.dev_folder_name
  depends_on = [time_sleep.wait_10_seconds]
}

resource "yandex_resourcemanager_folder" "prod" {
  cloud_id   = yandex_resourcemanager_cloud.cloud.id
  name       = var.prod_folder_name
  depends_on = [time_sleep.wait_10_seconds]
}

# S3 bucket

# Create SA
resource "yandex_iam_service_account" "tfstatesa" {
  folder_id = yandex_resourcemanager_folder.mgmt.id
  name      = var.tfstatesa_name
}

# Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "tfstatesa-editor" {
  folder_id = yandex_resourcemanager_folder.mgmt.id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.tfstatesa.id}"
}

# Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "tfstatesa-static-key" {
  service_account_id = yandex_iam_service_account.tfstatesa.id
  description        = "static access key for object storage"
}

# Use keys to create bucket
resource "yandex_storage_bucket" "tfstatebucket" {
  folder_id  = yandex_resourcemanager_folder.mgmt.id
  access_key = yandex_iam_service_account_static_access_key.tfstatesa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.tfstatesa-static-key.secret_key
  bucket     = var.tfstatebucket_name
  max_size   = var.tfstatebucket_maxsize
}

# SA for cloud management
resource "yandex_iam_service_account" "tfmgmtsa" {
  folder_id = yandex_resourcemanager_folder.mgmt.id
  name      = var.tfmgmtsa_name
}

resource "yandex_resourcemanager_cloud_iam_member" "tfmgmtsa-editor" {
  cloud_id = yandex_resourcemanager_cloud.cloud.id
  role     = "editor"
  member   = "serviceAccount:${yandex_iam_service_account.tfmgmtsa.id}"
}

resource "yandex_iam_service_account_key" "iamserviceaccount_key" {
  service_account_id = yandex_iam_service_account.tfmgmtsa.id
}


resource "local_sensitive_file" "iamserviceaccount_key" {
  count = var.write_keys_to_file ? 1 : 0
  content = templatefile(
    "${path.module}/templates/key.tpl.json",
    {
      key_id             = yandex_iam_service_account_key.iamserviceaccount_key.id
      service_account_id = yandex_iam_service_account_key.iamserviceaccount_key.service_account_id
      created_at         = yandex_iam_service_account_key.iamserviceaccount_key.created_at
      key_algorithm      = yandex_iam_service_account_key.iamserviceaccount_key.key_algorithm
      public_key         = jsonencode(yandex_iam_service_account_key.iamserviceaccount_key.public_key)
      private_key        = jsonencode(yandex_iam_service_account_key.iamserviceaccount_key.private_key)
    }
  )
  filename = "iamserviceaccount_key.json"
}

resource "local_sensitive_file" "tfstatesa-static-key" {
  count = var.write_keys_to_file ? 1 : 0
  content = templatefile(
    "${path.module}/templates/s3key.tpl.txt",
    {
      access_key = yandex_iam_service_account_static_access_key.tfstatesa-static-key.access_key
      secret_key = yandex_iam_service_account_static_access_key.tfstatesa-static-key.secret_key
    }
  )
  filename = "s3keys.txt"
}
